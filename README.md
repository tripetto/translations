# 🌐 Tripetto Translations
This repository contains an overview of all translatable components of Tripetto. The table below includes a link to the POT file for each component, together with instructions to test your translation and instructions to submit it. Also, we track the state of each language for each component, so we have a good overview of the translation process.

### Tools
You can use any tool or service you like, but internally we use [Poedit](https://poedit.net/) to create translations. To download Poedit for Windows or Mac, go to: https://poedit.net/download. To install Poedit on Linux, execute:
```
$ sudo apt-get install poedit
```

### Translation guide
To create a translation, follow these steps:
1. Download the `POT`-file for the component you want to translate;
2. Create your translation using your favorite translation tool or service;
3. Test the translation (see [requirements](#requirements) for your test machine);
4. Submit the translation;
5. Update the languages table in this `README` file. If you are the first for a translation locale, add a new column for the new locale. Update the translation state of each component for your locale;
6. Submit an MR with the updated `README` file.

### Requirements
We use [gettext](https://www.gnu.org/software/gettext/) to generate the translation template files. If you are on Windows, the instructions to test your translations should work out of the box (we include the `xgettext` binary). If you are on Linux or MacOS, make sure to install gettext.

On Linux run:
```bash
$ apt-get install -y gettext
```

On MacOS run:
```bash
$ brew install gettext
```

Make sure gettext works from your command line/terminal. You can test this by entering `xgettext --version` in your terminal. It should emit the version number of gettext. You should use version `0.19.8.1` or higher.

## Components, languages and translation progress
##### ✔ Complete ⚠ Incomplete/needs work ❌ Missing

|                  | POT                                                                                                    | TEST                                        | SUBMIT                                        | en | de | es | fr | id | nl | pl | pt | pt_BR |
|------------------|:------------------------------------------------------------------------------------------------------:|:-------------------------------------------:|:---------------------------------------------:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:-----:|
| **CORE**         |                                                                                                        |                                             |                                               |    |    |    |    |    |    |    |    |       |
| Builder          | [Download](https://unpkg.com/@tripetto/builder/translations/template.pot)                              | [Instructions](#test-builder-translation)   | [Instructions](#submit-builder-translation)   | ✔  | ⚠  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ❌     |
| **RUNNERS**      |                                                                                                        |                                             |                                               |    |    |    |    |    |    |    |    |       |
| Autoscroll       | [Download](https://unpkg.com/@tripetto/runner-autoscroll/runner/translations/template.pot)             | [Instructions](#test-runner-translation)    | [Instructions](#submit-runner-translation)    | ✔  | ✔  | ✔  | ✔  | ✔  | ✔  | ⚠  | ✔  | ❌     |
| Chat             | [Download](https://unpkg.com/@tripetto/runner-chat/runner/translations/template.pot)                   | [Instructions](#test-runner-translation)    | [Instructions](#submit-runner-translation)    | ✔  | ✔  | ✔  | ✔  | ✔  | ✔  | ❌  | ✔  | ❌     |
| Classic          | [Download](https://unpkg.com/@tripetto/runner-classic/runner/translations/template.pot)                | [Instructions](#test-runner-translation)    | [Instructions](#submit-runner-translation)    | ✔  | ✔  | ✔  | ✔  | ✔  | ✔  | ❌  | ✔  | ❌     |
| **BLOCKS**       |                                                                                                        |                                             |                                               |    |    |    |    |    |    |    |    |       |
| Calculator       | [Download](https://unpkg.com/@tripetto/block-calculator/translations/template.pot)                     | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ⚠  | ✔  | ❌  | ❌  | ❌     |
| Checkbox         | [Download](https://unpkg.com/@tripetto/block-checkbox/translations/template.pot)                       | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ⚠  | ❌  | ❌     |
| Checkboxes       | [Download](https://unpkg.com/@tripetto/block-checkboxes/translations/template.pot)                     | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ❌     |
| Date             | [Download](https://unpkg.com/@tripetto/block-date/translations/template.pot)                           | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ❌  | ❌  | ❌     |
| Device           | [Download](https://unpkg.com/@tripetto/block-device/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Dropdown         | [Download](https://unpkg.com/@tripetto/block-dropdown/translations/template.pot)                       | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ❌     |
| Email            | [Download](https://unpkg.com/@tripetto/block-email/translations/template.pot)                          | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ❌  | ❌  | ❌     |
| Error            | [Download](https://unpkg.com/@tripetto/block-error/translations/template.pot)                          | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Evaluate         | [Download](https://unpkg.com/@tripetto/block-evaluate/translations/template.pot)                       | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| File upload      | [Download](https://unpkg.com/@tripetto/block-file-upload/translations/template.pot)                    | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Hidden field     | [Download](https://unpkg.com/@tripetto/block-hidden-field/translations/template.pot)                   | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ⚠  | ❌  | ❌     |
| Mailer           | [Download](https://unpkg.com/@tripetto/block-mailer/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Matrix           | [Download](https://unpkg.com/@tripetto/block-matrix/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ⚠  | ❌  | ❌     |
| Multi-select     | [Download](https://unpkg.com/@tripetto/block-multi-select/translations/template.pot)                   | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ❌  | ❌  | ❌  | ❌  | ✔  | ❌  | ❌  | ❌     |
| Multiple choice  | [Download](https://unpkg.com/@tripetto/block-multiple-choice/translations/template.pot)                | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ⚠  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ❌     |
| Number           | [Download](https://unpkg.com/@tripetto/block-number/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ⚠  | ❌  | ❌     |
| Paragraph        | [Download](https://unpkg.com/@tripetto/block-paragraph/translations/template.pot)                      | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Password         | [Download](https://unpkg.com/@tripetto/block-password/translations/template.pot)                       | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Phone number     | [Download](https://unpkg.com/@tripetto/block-phone-number/translations/template.pot)                   | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Picture choice   | [Download](https://unpkg.com/@tripetto/block-picture-choice/translations/template.pot)                 | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ⚠  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ❌     |
| Radiobuttons     | [Download](https://unpkg.com/@tripetto/block-radiobuttons/translations/template.pot)                   | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ❌     |
| Rating           | [Download](https://unpkg.com/@tripetto/block-rating/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ⚠  | ❌  | ❌     |
| Regex            | [Download](https://unpkg.com/@tripetto/block-regex/translations/template.pot)                          | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Scale            | [Download](https://unpkg.com/@tripetto/block-scale/translations/template.pot)                          | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ⚠  | ❌  | ❌     |
| Setter           | [Download](https://unpkg.com/@tripetto/block-setter/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ❌  | ✔  | ❌  | ❌  | ❌     |
| Statement        | [Download](https://unpkg.com/@tripetto/block-statement/translations/template.pot)                      | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Stop             | [Download](https://unpkg.com/@tripetto/block-stop/translations/template.pot)                           | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ❌  | ❌  | ❌     |
| Text             | [Download](https://unpkg.com/@tripetto/block-text/translations/template.pot)                           | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ❌  | ❌  | ❌     |
| Textarea         | [Download](https://unpkg.com/@tripetto/block-textarea/translations/template.pot)                       | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ❌  | ❌  | ❌     |
| URL              | [Download](https://unpkg.com/@tripetto/block-url/translations/template.pot)                            | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ✔  | ❌  | ❌     |
| Variable         | [Download](https://unpkg.com/@tripetto/block-variable/translations/template.pot)                       | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ✔  | ❌  | ❌  | ✔  | ✔  | ❌  | ❌  | ❌     |
| Yes/No           | [Download](https://unpkg.com/@tripetto/block-yes-no/translations/template.pot)                         | [Instructions](#test-block-translation)     | [Instructions](#submit-block-translation)     | ✔  | ⚠  | ❌  | ❌  | ⚠  | ✔  | ⚠  | ❌  | ⚠     |
| **PLATFORMS**    |                                                                                                        |                                             |                                               |    |    |    |    |    |    |    |    |       |
| Studio App       | [Download](https://gitlab.com/tripetto/studio/-/raw/production/translations/template.pot?inline=false) | [Instructions](#test-studio-translation)    | [Instructions](#submit-studio-translation)    | ✔  | ❌  | ❌  | ❌  | ✔  | ⚠  | ❌  | ❌  | ❌     |
| WordPress Plugin | [Download](https://gitlab.com/tripetto/wordpress/-/raw/master/translations/tripetto.pot?inline=false)  | [Instructions](#test-wordpress-translation) | [Instructions](#submit-wordpress-translation) | ✔  | ❌  | ❌  | ⚠  | ⚠  | ✔  | ❌  | ❌  | ❌     |

## Testing translations
During the process of translating, it helps to see your translation work in action. This section contains instructions to get the components up and running with your translation.

---
### Test builder translation
1. Install the Tripetto builder globally by running: `npm i @tripetto/builder -g` (you need [Node.js](https://nodejs.org/en/) to run this)
2. Install [po2jon](https://www.npmjs.com/package/po2json) globally by running: `npm i po2json -g`
3. Create your PO translation file
4. Run `po2json <locale>.po <locale>.json` for your translation file to convert it to JSON
5. Start the builder with your translation: `tripetto --language <locale>.json example-form.json` (make sure to start Tripetto in the same folder as your translation JSON, or specify a valid path to your JSON file).

---
### Test runner translation
> Important: Make sure to check the [requirements](#requirements).
1. Clone the runner repository: `git clone https://gitlab.com/tripetto/runners/<runner>.git` (for example `git clone https://gitlab.com/tripetto/runners/autoscroll.git`)
2. Run `npm i` to install the dependencies (you need [Node.js](https://nodejs.org/en/) to run this)
3. Add your PO translation file to the `./translations` folder (it should have the name `<locale>.po`, for example `nl.po`)
4. Run `npm test`
5. Go to `http://localhost:9000` to test the translation (click the translation button on the top right corner of the test application to switch languages).

---
### Test block translation
> Important: Make sure to check the [requirements](#requirements).
1. Clone the block repository: `git clone https://gitlab.com/tripetto/blocks/<block>.git` (for example `git clone https://gitlab.com/tripetto/blocks/checkbox.git`)
2. Run `npm i` to install the dependencies (you need [Node.js](https://nodejs.org/en/) to run this)
3. Add your PO translation file to the `./translations` folder (it should have the name `<locale>.po`, for example `nl.po`)
4. Edit `package.json` and search and replace the following string `tripetto ./test/example.json` with `tripetto --language <locale> ./test/example.json` (for example `tripetto --language nl ./test/example.json`)
5. Run `npm test`. A browser window should appear automically. If not go to `http://localhost:3333`.

---
### Test Studio translation
Currently, it is not possible to locally run the Studio SaaS application. This is related to the required GCP environment you need to configure. We currently don't offer public documentation for that for now (we hope to change that in the future). Please try to translate as much as possible and then submit a [MR](#submit-studio-translation). We will merge that into our staging environment and give you access to check the translation.

---
### Test WordPress translation
> Important: Make sure to check the [requirements](#requirements).
1. Clone the WordPress repository: `git clone https://gitlab.com/tripetto/wordpress.git`
2. Follow [these](https://gitlab.com/tripetto/wordpress/-/blob/master/README.md) instructions to get the WordPress environment up and running;
3. Add your PO and MO translation file to the `./translations` folder (it should have the name `<locale>.po` and `<locale>.mo`, for example `nl.po` and `nl.mo`)
4. Run `npm test`
5. Open the WP admin (make sure to switch the WP Admin to the language to test).


## Submitting translations
When your translation is finished, you can submit it. This section contains instructions to do that.

---
### Submit builder translation
The builder is currently closed source. Please send your translation to support@tripetto.com so we can add it to the repository for you. We'll make sure to give props to you by including your name in the readme of the builder.

---
### Submit runner translation
1. Clone the runner repository: `git clone https://gitlab.com/tripetto/runners/<runner>.git` (for example `git clone https://gitlab.com/tripetto/runners/autoscroll.git`)
2. Create a new branch with the name `translation/<locale>` (for example `translation/nl`)
3. Add your PO translation file to the `./translations` folder (it should have the name `<locale>.po`, for example `nl.po`)
4. Commit your initial translation version with the message `Add <language> translation` (for example `Add dutch translation`) and translation updates with `Update <language> translation`
5. Push your commit and submit a merge request here: `https://gitlab.com/tripetto/runners/<runner>/-/merge_requests`

> Note: You need a [GitLab-account](https://gitlab.com/users/sign_up) to commit to the repository and submit a MR.

---
### Submit block translation
1. Clone the block repository: `git clone https://gitlab.com/tripetto/blocks/<block>.git` (for example `git clone https://gitlab.com/tripetto/blocks/checkbox.git`)
2. Create a new branch with the name `translation/<locale>` (for example `translation/nl`)
3. Add your PO translation file to the `./translations` folder (it should have the name `<locale>.po`, for example `nl.po`)
4. Commit your initial translation version with the message `Add <language> translation` (for example `Add dutch translation`) and translation updates with `Update <language> translation`
5. Push your commit and submit a merge request here: `https://gitlab.com/tripetto/blocks/<block>/-/merge_requests`

> Note: You need a [GitLab-account](https://gitlab.com/users/sign_up) to commit to the repository and submit a MR.

---
### Submit Studio translation
1. Clone the Studio repository: `git clone https://gitlab.com/tripetto/studio.git`
2. Create a new branch with the name `translation/<locale>` (for example `translation/nl`)
3. Add your PO translation file to the `./translations` folder (it should have the name `<locale>.po`, for example `nl.po`)
4. Commit your initial translation version with the message `Add <language> translation` (for example `Add dutch translation`) and translation updates with `Update <language> translation`
5. Push your commit and submit a merge request here: `https://gitlab.com/tripetto/studio/-/merge_requests`

> Note: You need a [GitLab-account](https://gitlab.com/users/sign_up) to commit to the repository and submit a MR.

---
### Submit WordPress translation
1. Clone the WordPress repository: `git clone https://gitlab.com/tripetto/wordpress.git`
2. Create a new branch with the name `translation/<locale>` (for example `translation/nl`)
3. Add your PO and MO translation file to the `./translations` folder (it should have the name `<locale>.po` and `<locale>.mo`, for example `nl.po` and `nl.mo`)
4. Commit your initial translation version with the message `Add <language> translation` (for example `Add dutch translation`) and translation updates with `Update <language> translation`
5. Push your commit and submit a merge request here: `https://gitlab.com/tripetto/wordpress/-/merge_requests`
6. Submit your translation to the WordPress translation website: https://translate.wordpress.org/projects/wp-plugins/tripetto/
7. Ask a WordPress translation editor for your locale to review the translation.

> Note: You need a [GitLab-account](https://gitlab.com/users/sign_up) to commit to the repository and submit a MR.

> Note: You need a [WordPress-account](https://login.wordpress.org/register?locale=en_US) to add translations to the WordPress plugin repository.


## Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (id)
- [Julian Frauenholz](https://gitlab.com/frauenholz) (de)
- [Krzysztof Kamiński](https://gitlab.com/kriskaminski) (pl)
- [Gustavo RPS](https://gitlab.com/gustavorps) (pt_BR)

🙏 Thank you very much for your efforts! You make Tripetto better, and that's awesome.

## Quality control
We do our best to ensure the quality of the translations. We reserve the right to refuse translation merge requests that we consider of insufficient quality.
